package com.thirautech.sams.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.thirautech.sams.vo.CodeVo;

@Repository("codeDao")
public class CodeDao {

	@Autowired
	private SqlSession sqlSession;
	
	public List<CodeVo> selectCodeByGubun(CodeVo codeVo) throws Exception {
		return sqlSession.selectList("com.thirautech.sams.selectCodeByGubun", codeVo);
	}
	
	public List<CodeVo> selectCodeByPCode(CodeVo codeVo) throws Exception {
		return sqlSession.selectList("com.thirautech.sams.selectCodeByPCode", codeVo);
	}
}
